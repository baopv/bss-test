<?php

namespace Bss\Newsletter\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Bss\Newsletter\Ui\Component\Listing\ColumnDelete
 */
class Delete extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Constructor.
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$fieldName.'_html'] = "<button class='button'><span>".__('Delete')."</span></button>";
                $item[$fieldName.'_title'] = __('What is the reason to delete this?');
                $item[$fieldName.'_submitlabel'] = __('Delete');
                $item[$fieldName.'_cancellabel'] = __('Reset');
                $item[$fieldName.'_newsletterid'] = $item['entity_id'];

                $item[$fieldName.'_formaction'] = $this->urlBuilder->getUrl('bss/newsletter/delete');
            }
        }

        return $dataSource;
    }
}
