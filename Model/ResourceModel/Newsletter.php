<?php

namespace Bss\Newsletter\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Newsletter extends AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('bss_newsletter_subscriber', 'entity_id');
    }
}
