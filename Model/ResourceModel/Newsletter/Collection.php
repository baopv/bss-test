<?php

namespace Bss\Newsletter\Model\ResourceModel\Newsletter;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Bss\Newsletter\Model\ResourceModel\Newsletter/Collection
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Bss\Newsletter\Model\Newsletter::class,
            \Bss\Newsletter\Model\ResourceModel\Newsletter::class
        );
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinInner(
            ['customerTable' => $this->getTable('customer_entity')],
            'main_table.customer_id = customerTable.entity_id',
            [
                'firstname' => 'customerTable.firstname',
                'middlename' => 'customerTable.middlename',
                'lastname' => 'customerTable.lastname',
                'gender' => 'customerTable.gender'
            ]
        );
        $this->addFilterToMap('entity_id', 'main_table.entity_id');
        return $this;
    }
}
