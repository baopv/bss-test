<?php

namespace Bss\Newsletter\Model\Config\Source;

/**
 * Class Bss\Newsletter\Model\Config\Source\Status
 */
class Status implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Subscribed')],
            ['value' => 2, 'label' =>__('Not Activated')],
            ['value' => 3, 'label' =>__('Unsubscribed')],
            ['value' => 4, 'label' =>__('Unconfirmed')]
        ];
    }

//    /**
//     * Get options in "key-value" format
//     *
//     * @return array
//     */
////    public function toArray()
////    {
////        return [
////            1 => __('Subscribed'),
////            2 => __('Not Activated'),
////            3 => __('Unsubscribed'),
////            4 => __('Unconfirmed')
////        ];
////    }
}
