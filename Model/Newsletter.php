<?php

namespace Bss\Newsletter\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Bss\Newsletter\Model\Newsletter
 */
class Newsletter extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Bss\Newsletter\Model\ResourceModel\Newsletter::class);
    }
}
