<?php

namespace Bss\Newsletter\Controller\Adminhtml\Newsletter;

use Bss\Newsletter\Model\Newsletter;
use Bss\Newsletter\Model\NewsletterFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Bss\Newsletter\Controller\Adminhtml\Newsletter\InlineEdit
 */
class InlineEdit extends Action implements HttpPostActionInterface
{
    /**
     * @var \Magento\Cms\Controller\Adminhtml\Page\PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    protected $pageRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var NewsletterFactory
     */
    private $newsletterFactory;

    /**
     * InlineEdit constructor.
     *
     * @param Context $context
     * @param PostDataProcessor $dataProcessor
     * @param NewsletterFactory $newsletterFactory
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        PostDataProcessor $dataProcessor,
        NewsletterFactory $newsletterFactory,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->dataProcessor = $dataProcessor;
        $this->newsletterFactory = $newsletterFactory;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Process the request
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData(
                [
                    'messages' => [__('Please correct the data sent.')],
                    'error' => true,
                ]
            );
        }

        foreach (array_keys($postItems) as $newsletterId) {
            /** @var Newsletter $newsletter */
            $newsletter = $this->newsletterFactory->create()->load($newsletterId);
            try {
                $newsletterData = $this->filterPost($postItems[$newsletterId]);
                $this->validatePost($newsletterData, $newsletter, $error, $messages);
                $extendedPageData = $newsletter->getData();
                $this->setNewsletterPageData($newsletter, $extendedPageData, $newsletterData);
                $newsletter->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithNewsletterId($newsletter, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithNewsletterId($newsletter, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithNewsletterId(
                    $newsletter,
                    __('Something went wrong while saving the page.')
                );
                $error = true;
            }
        }

        return $resultJson->setData(
            [
                'messages' => $messages,
                'error' => $error
            ]
        );
    }

    /**
     * Filtering posted data.
     *
     * @param array $postData
     * @return array
     */
    protected function filterPost($postData = [])
    {
        $pageData = $this->dataProcessor->filter($postData);
        return $pageData;
    }

    /**
     * Validate post data
     *
     * @param array $pageData
     * @param Newsletter $newsletter
     * @param bool $error
     * @param array $messages
     * @return void
     */
    protected function validatePost(array $pageData, Newsletter $newsletter, &$error, array &$messages)
    {
        if (!$this->dataProcessor->validateRequireEntry($pageData)) {
            $error = true;
            foreach ($this->messageManager->getMessages(true)->getItems() as $error) {
                $messages[] = $this->getErrorWithNewsletterId($newsletter, $error->getText());
            }
        }
    }

    /**
     * Add page title to error message
     *
     * @param  Newsletter $newsletter
     * @param  string $errorText
     * @return string
     */
    protected function getErrorWithNewsletterId($newsletter, $errorText)
    {
        return '[Page ID: ' . $newsletter->getId() . '] ' . $errorText;
    }

    /**
     * Set cms page data
     *
     * @param Newsletter $newsletter
     * @param array $extendedPageData
     * @param array $pageData
     * @return $this
     */
    public function setNewsletterPageData(Newsletter $newsletter, array $extendedPageData, array $pageData)
    {
        $newsletter->setData(array_merge($newsletter->getData(), $extendedPageData, $pageData));
        return $this;
    }
}
